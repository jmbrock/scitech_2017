\begin{thebibliography}{10}
\newcommand{\enquote}[1]{``#1''}

\bibitem{kazemba2016survey}
Kazemba, C.~D., Braun, R.~D., Clark, I.~G., and Schoenenberger, M.,
  \enquote{Survey of Blunt-Body Supersonic Dynamic Stability,} {\em Journal of
  Spacecraft and Rockets\/}, 2016, pp.~1--19.

\bibitem{Murman:2009es}
Murman, S.~M., \enquote{{Dynamic Simulations of Atmospheric-Entry Capsules},}
  {\em Journal of Spacecraft and Rockets\/}, Vol.~46, No.~4, July 2009,
  pp.~1--7.

\bibitem{Teramoto:2001up}
Teramoto, S., Hiraki, K., and Fujii, K., \enquote{{Numerical Analysis of
  Dynamic Stability of a Reentry Capsule at Transonic Speeds},} {\em AIAA
  Journal\/}, Vol.~39, No.~4, April 2001, pp.~1--8.

\bibitem{Teramoto:2002hu}
Teramoto, S. and Fujii, K., \enquote{{Mechanism of Dynamic Instability of a
  Reentry Capsule at Transonic Speeds},} {\em AIAA Journal\/}, Vol.~40, No.~12,
  Dec. 2002, pp.~2467--2475.

\bibitem{Stern:2012wc}
Stern, E.~C., Gidzak, V.~M., and Candler, G.~V., \enquote{{Estimation of
  Dynamic Stability Coefficients for Aerodynamic Decelerators Using CFD},} {\em
  30th AIAA Applied Aerodynamics Conference\/}, New Orleans, June 2012, pp.
  1--14.

\bibitem{Dutta:2015wo}
Dutta, S., Bowes, A.~L., Striepe, S.~A., Davis, J.~L., Queen, E.~M., Blood,
  E.~M., and Ivanov, M.~C., \enquote{{Supersonic Flight Dynamics Test 1 -
  Post-Flight Assessment of Simulation Performance},} {\em AAS/AIAA Space
  Flight Mechanics Conference\/}, Williamsburg, Virginia, Jan. 2015.

\bibitem{Wilder:2016}
Wilder, M.~C., Bogdanoff, D.~W., Yates, L.~A., and Dyakonov, A.~A.,
  \enquote{{Aerodynamic Coefficients from Aeroballistic Range Testing of
  Deployed- and Stowed-SIAD SFDT Models},} .

\bibitem{brown2010transonic}
Brown, J.~D., Bogdanoff, D.~W., Yates, L.~A., and Chapman, G.~T.,
  \enquote{Transonic Aerodynamics of a Lifting Orion Crew Capsule from
  Ballistic Range Data,} {\em Journal of Spacecraft and Rockets\/}, Vol.~47,
  No.~1, 2010, pp.~36--47.

\bibitem{canning1970ballistic}
Canning, T.~N., Seiff, A., and James, C.~S., \enquote{Ballistic-range
  technology,} Tech. rep., DTIC Document, 1970.

\bibitem{yates1996comprehensive}
Yates, L., \enquote{A Comprehensive Automated Aerodynamic Reduction System for
  Ballistic Ranges,} {\em US Air Force Wright Laboratory, Rept.
  WL-TR-96-7059\/}, 1996.

\bibitem{Yates:1995vu}
Yates, L.~A., {\em {CADRA System User's Guide}\/}, Aerospace Computing, Inc.,
  1995.

\bibitem{Nompelis:2004vg}
Nompelis, I., Drayna, T.~W., and Candler, G.~V., \enquote{{Development of a
  Hybrid Unstructured Implicit Solver for the Simulation of Reacting Flows Over
  Complex Geometries },} {\em 34th AIAA Fluid Dynamics Conference\/}, The
  University of Minnesota, Portland, July 2004, pp. 1--12.

\bibitem{Nompelis:2005wo}
Nompelis, I., Drayna, T.~W., and Candler, G.~V., \enquote{{A Parallel
  Unstructured Implicit Solver for Hypersonic Reacting Flow Simulation},} {\em
  17th AIAA Computational Fluid Dynamics Conference\/}, The University of
  Minnesota, Toronto, July 2005, pp. 1--17.

\bibitem{Wright:1997ws}
Wright, M.~J., {\em {A Family of Data-Parallel Relaxation Methods for the
  Navier-Stokes Equations}\/}, Ph.D. thesis, University of Minnesota, June
  1997.

\bibitem{maccormack1989solution}
MacCormack, R.~W. and Candler, G.~V., \enquote{The solution of the
  Navier-Stokes equations using Gauss-Seidel line relaxation,} {\em Computers
  \& fluids\/}, Vol.~17, No.~1, 1989, pp.~135--150.

\bibitem{Subbareddy:2009ku}
Subbareddy, P.~K. and Candler, G.~V., \enquote{{A fully discrete, kinetic
  energy consistent finite-volume scheme for compressible flows},} {\em Journal
  of Computational Physics\/}, Vol.~228, No.~5, March 2009, pp.~1347--1364.

\bibitem{Brock:2014dy}
Brock, J.~M., Subbareddy, P.~K., and Candler, G.~V., \enquote{{Detached-Eddy
  Simulations of Hypersonic Capsule Wake Flow},} {\em AIAA Journal\/}, Nov.
  2014, pp.~1--11.

\bibitem{SPALART:2013dq}
Spalart, P. and Allmaris, S., \enquote{{A one-equation turbulence model for
  aerodynamic flows},} {\em 30th Aerospace Sciences Meeting and Exhibit\/},
  American Institute of Aeronautics and Astronautics, Reno, Nevada, June 1992,
  pp. 1--58.

\bibitem{Catris:2000dr}
Catris, S. and Aupoix, B., \enquote{{Density corrections for turbulence
  models},} {\em Aerospace Science and Technology\/}, Vol.~4, No.~1, 2000,
  pp.~1--11.

\end{thebibliography}
